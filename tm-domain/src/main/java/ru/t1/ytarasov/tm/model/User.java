package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USUAL;

    private boolean isLocked = false;

    public User(@Nullable final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@Nullable final String login, @Nullable final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(@Nullable final String login, @Nullable final String passwordHash, @Nullable final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @Override
    public String toString() {
        return login;
    }

}
