package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ytarasov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.dto.request.user.*;
import ru.t1.ytarasov.tm.dto.response.user.*;
import ru.t1.ytarasov.tm.marker.SoapCategory;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.UUID;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final String ADMIN_USER_LOGIN = "ADMIN";

    @NotNull
    private static final String ADMIN_USER_PASSWORD = "ADMIN";

    @NotNull
    private static final String TEST_USER_LOGIN = "TEST";

    @NotNull
    private static final String TEST_USER_PASSWORD = "TEST";

    @NotNull
    private static final String USER_EMAIL = "test@mail.ru";

    @NotNull
    private static final String NEW_USER_LOGIN = "TEST1";

    @NotNull
    private static final String NEW_USER_PASSWORD = "TEST1";

    @NotNull
    private static final String NEW_USER_EMAIL = "test1@mail.ru";

    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();

    @Nullable
    private static final String NULL_TOKEN = null;

    @Nullable
    private String adminToken;

    @Nullable
    private String testToken;

    @Before
    public void setup(){
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        adminToken = authEndpoint.login(adminLoginRequest).getToken();
        @NotNull final UserLoginRequest testLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        testToken = authEndpoint.login(testLoginRequest).getToken();
    }

    @After
    public void cleanup(){
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(adminLogoutRequest);
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(testLogoutRequest);
    }

    @Test
    public void registryUser(){
        @NotNull final UserRegistryRequest request1 = new UserRegistryRequest();
        request1.setLogin(null);
        request1.setPassword(NEW_USER_PASSWORD);
        request1.setEmail(NEW_USER_EMAIL);
        @NotNull final UserRegistryRequest request2 = new UserRegistryRequest();
        request2.setLogin(NEW_USER_LOGIN);
        request2.setPassword(null);
        request2.setEmail(NEW_USER_EMAIL);
        @NotNull final UserRegistryRequest request3 = new UserRegistryRequest();
        request3.setLogin(NEW_USER_LOGIN);
        request3.setPassword(NEW_USER_PASSWORD);
        request3.setEmail(null);
        @NotNull final UserRegistryRequest request4 = new UserRegistryRequest();
        request4.setLogin(TEST_USER_LOGIN);
        request4.setPassword(NEW_USER_PASSWORD);
        request4.setEmail(NEW_USER_EMAIL);
        @NotNull final UserRegistryRequest request5 = new UserRegistryRequest();
        request5.setLogin(NEW_USER_LOGIN);
        request5.setPassword(NEW_USER_PASSWORD);
        request5.setEmail(USER_EMAIL);
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(request1));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(request2));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(request3));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(request4));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(request5));
        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest();
        userRegistryRequest.setLogin(NEW_USER_LOGIN);
        userRegistryRequest.setPassword(NEW_USER_PASSWORD);
        userRegistryRequest.setEmail(NEW_USER_EMAIL);
        @NotNull final UserRegistryResponse registryResponse = userEndpoint.registryUser(userRegistryRequest);
        Assert.assertNotNull(registryResponse);
        Assert.assertNotNull(registryResponse.getUser());
        @NotNull final UserRemoveRequest userRemoveRequest = new UserRemoveRequest(adminToken);
        userEndpoint.removeUser(userRemoveRequest);
    }

    @Test
    public void removeUser(){
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(NEW_USER_LOGIN);
        registryRequest.setPassword(NEW_USER_PASSWORD);
        registryRequest.setEmail(NEW_USER_EMAIL);
        userEndpoint.registryUser(registryRequest);
        @NotNull final UserRemoveRequest request1 = new UserRemoveRequest();
        @NotNull final UserRemoveRequest request2 = new UserRemoveRequest(NULL_TOKEN);
        @NotNull final UserRemoveRequest request3 = new UserRemoveRequest(BAD_TOKEN);
        @NotNull final UserRemoveRequest request4 = new UserRemoveRequest(testToken);
        Assert.assertThrows(Exception.class, ()-> userEndpoint.removeUser(request1));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.removeUser(request2));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.removeUser(request3));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.removeUser(request4));
        @NotNull final UserRemoveRequest userRemoveRequest = new UserRemoveRequest(adminToken);
        userRemoveRequest.setLogin(NEW_USER_LOGIN);
        Assert.assertNotNull(userEndpoint.removeUser(userRemoveRequest));
    }

    @Test
    public void lockUser(){
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin(null);
        Assert.assertThrows(Exception.class, ()-> userEndpoint.lockUser(new UserLockRequest()));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.lockUser(new UserLockRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.lockUser(new UserLockRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.lockUser(new UserLockRequest(testToken)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.lockUser(request));
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin(TEST_USER_LOGIN);
        @NotNull final UserLockResponse lockResponse = userEndpoint.lockUser(lockRequest);
        Assert.assertNotNull(lockResponse);
        Assert.assertTrue(lockResponse.getSuccess());
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        userEndpoint.unlockUser(unlockRequest);
    }

    @Test
    public void unlockUser(){
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin(TEST_USER_LOGIN);
        userEndpoint.lockUser(lockRequest);
        @NotNull final UserLockResponse lockResponse = userEndpoint.lockUser(lockRequest);
        Assert.assertNotNull(lockResponse);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(TEST_USER_LOGIN);
        userLoginRequest.setPassword(TEST_USER_PASSWORD);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest()));
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(adminToken);
        request.setLogin(null);
        Assert.assertThrows(Exception.class, ()-> userEndpoint.unlockUser(new UserUnlockRequest()));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.unlockUser(new UserUnlockRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.unlockUser(new UserUnlockRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.unlockUser(new UserUnlockRequest(testToken)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.unlockUser(request));
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin(TEST_USER_LOGIN);
        @NotNull final UserUnlockResponse unlockResponse = userEndpoint.unlockUser(unlockRequest);
        Assert.assertNotNull(unlockResponse);
    }

    @Test
    public void changeUserPassword(){
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse profileResponse = authEndpoint.profile(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileResponse.getUser());
        @Nullable final String oldPassword = profileResponse.getUser().getPasswordHash();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(testToken);
        request.setPassword(null);
        Assert.assertThrows(Exception.class, ()-> userEndpoint.changeUserPassword(new UserChangePasswordRequest()));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.changeUserPassword(new UserChangePasswordRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.changeUserPassword(new UserChangePasswordRequest(BAD_TOKEN)));
        Assert.assertThrows(
                Exception.class,
                ()-> userEndpoint.changeUserPassword(request)
        );
        @NotNull final UserChangePasswordRequest changePasswordRequest =
                new UserChangePasswordRequest(testToken);
        changePasswordRequest.setPassword(NEW_USER_PASSWORD);
        @NotNull final UserChangePasswordResponse changePasswordResponse =
                userEndpoint.changeUserPassword(changePasswordRequest);
        Assert.assertNotNull(changePasswordResponse);
        Assert.assertNotNull(changePasswordResponse.getUser());
        Assert.assertNotEquals(oldPassword, changePasswordResponse.getUser().getPasswordHash());
        @NotNull final String secret = "951753";
        @NotNull final Integer iteration = 3751;
        Assert.assertEquals(
                HashUtil.salt(NEW_USER_PASSWORD, secret, iteration),
                changePasswordResponse.getUser().getPasswordHash()
        );
        changePasswordRequest.setPassword(TEST_USER_PASSWORD);
        userEndpoint.changeUserPassword(changePasswordRequest);
    }

    @Test
    public void updateUserProfile(){
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest();
        profileRequest.setToken(testToken);
        @NotNull final UserProfileResponse profileResponse = authEndpoint.profile(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileResponse.getUser());
        @Nullable final String oldFirstName = profileResponse.getUser().getFirstName();
        @Nullable final String oldMiddleName = profileResponse.getUser().getMiddleName();
        @Nullable final String oldLastName = profileResponse.getUser().getLastName();
        @NotNull final String newFirstName = "TTT";
        @NotNull final String newMiddleName = "EEE";
        @NotNull final String newLastName = "WWW";
        Assert.assertThrows(Exception.class, ()-> userEndpoint.updateUserProfile( new UserUpdateProfileRequest()));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.updateUserProfile( new UserUpdateProfileRequest(null)));
        Assert.assertThrows(Exception.class, ()-> userEndpoint.updateUserProfile( new UserUpdateProfileRequest(BAD_TOKEN)));
        @NotNull final UserUpdateProfileRequest updateRequest =
                new UserUpdateProfileRequest(testToken);
        updateRequest.setFirstName(newFirstName);
        updateRequest.setMiddleName(newMiddleName);
        updateRequest.setLastName(newLastName);
        @NotNull final UserUpdateProfileResponse updateResponse = userEndpoint.updateUserProfile(updateRequest);
        Assert.assertNotNull(updateResponse);
        Assert.assertNotNull(updateResponse.getUser());
        Assert.assertNotEquals(oldFirstName, updateResponse.getUser().getFirstName());
        Assert.assertNotEquals(oldMiddleName, updateResponse.getUser().getMiddleName());
        Assert.assertNotEquals(oldLastName, updateResponse.getUser().getLastName());
        Assert.assertEquals(newFirstName, updateResponse.getUser().getFirstName());
        Assert.assertEquals(newMiddleName, updateResponse.getUser().getMiddleName());
        Assert.assertEquals(newLastName, updateResponse.getUser().getLastName());
        updateRequest.setFirstName(oldFirstName);
        updateRequest.setMiddleName(oldMiddleName);
        updateRequest.setLastName(oldLastName);
        userEndpoint.updateUserProfile(updateRequest);
    }

}
