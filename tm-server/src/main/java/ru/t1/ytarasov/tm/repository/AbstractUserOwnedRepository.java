package ru.t1.ytarasov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.repository.IUserOwnedRepository;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    private final String USER_ID_COLUMN = DBConstant.COLUMN_USER_ID;

    public AbstractUserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
            return result;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql;
        if (comparator == null) sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        else
            sql = String.format("SELECT * FROM %s WHERE %s = ? ORDER BY %s", getTableName(), USER_ID_COLUMN, getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND %s = ? LIMIT 1", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final String sql = String.format("SELECT COUNT(*) AS \"count\" FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final String userId, @NotNull final M model);

    @Override
    @SneakyThrows
    public M remove(@NotNull String userId, final @NotNull M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? AND %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return model;
    }
}
