package ru.t1.ytarasov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_USER;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstant.COLUMN_ID));
        user.setLogin(row.getString(DBConstant.COLUMN_LOGIN));
        user.setPasswordHash(row.getString(DBConstant.COLUMN_PASSWORD));
        user.setEmail(row.getString(DBConstant.COLUMN_EMAIL));
        user.setFirstName(row.getString(DBConstant.COLUMN_FIRST_NAME));
        user.setMiddleName(row.getString(DBConstant.COLUMN_MIDDLE_NAME));
        user.setLastName(row.getString(DBConstant.COLUMN_LAST_NAME));
        user.setRole(Role.toRole(row.getString(DBConstant.COLUMN_ROLE)));
        final int isLocked = row.getInt(DBConstant.COLUMN_LOCKED);
        user.setLocked(isLocked > 0);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull User model) {
        final int isLocked = model.isLocked() ? 1 : 0;
        @NotNull final String sql = String.format("INSERT INTO %s(%s, %s, %s, %s, %s, %s, %s, %s, %s) " +
                        "VALUES " +
                        "(?, ?, ?, ?, ?, ?, ?, ?, ?)", getTableName(), DBConstant.COLUMN_ID, DBConstant.COLUMN_LOGIN,
                DBConstant.COLUMN_PASSWORD, DBConstant.COLUMN_EMAIL, DBConstant.COLUMN_LAST_NAME, DBConstant.COLUMN_FIRST_NAME,
                DBConstant.COLUMN_MIDDLE_NAME, DBConstant.COLUMN_ROLE, DBConstant.COLUMN_LOCKED);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getEmail());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getFirstName());
            statement.setString(7, model.getMiddleName());
            statement.setString(8, model.getRole().getDisplayName());
            statement.setInt(9, isLocked);
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String passwordHash) {
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login,
                       @NotNull final String passwordHash,
                       @NotNull final String email) {
        @NotNull User user = new User(login, passwordHash);
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login,
                       @NotNull final String passwordHash,
                       @NotNull final Role role) {
        @NotNull User user = new User(login, passwordHash);
        user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email, @NotNull Role role) {
        @NotNull final User user = new User(login, passwordHash, email);
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstant.COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstant.COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_LOGIN);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format("UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstant.COLUMN_PASSWORD, DBConstant.COLUMN_EMAIL, DBConstant.COLUMN_LAST_NAME,
                DBConstant.COLUMN_FIRST_NAME, DBConstant.COLUMN_MIDDLE_NAME, DBConstant.COLUMN_ROLE, DBConstant.COLUMN_LOCKED,
                DBConstant.COLUMN_ID);
        final int isLocked = user.isLocked() ? 1 : 0;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getPasswordHash());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getRole().toString());
            statement.setInt(7, isLocked);
            statement.setString(8, user.getId());
            statement.executeUpdate();
            return user;
        }
    }

}
