package ru.t1.ytarasov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull String getTableName() {
        return DBConstant.TABLE_PROJECT;
    }

    @Override
    @SneakyThrows
    protected @NotNull Project fetch(@NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstant.COLUMN_ID));
        project.setName(row.getString(DBConstant.COLUMN_NAME));
        project.setCreated(row.getTimestamp(DBConstant.COLUMN_CREATED));
        project.setDescription(row.getString(DBConstant.COLUMN_DESCRIPTION));
        project.setUserId(row.getString(DBConstant.COLUMN_USER_ID));
        project.setStatus(Status.valueOf(row.getString(DBConstant.COLUMN_STATUS)));
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        Project project = new Project(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        Project project = new Project(name, status);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name,
                          @NotNull final String description, Status status) {
        Project project = new Project(name, description, status);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    @SneakyThrows
    public @Nullable Project update(@NotNull Project project) {
        @NotNull final String name = project.getName();
        @NotNull final String description = project.getDescription();
        @NotNull final Status status = project.getStatus();
        @NotNull final String id = project.getId();
        @Nullable final String userId = project.getUserId();
        if (userId == null) return null;
        @NotNull final String sql = String.format("UPDATE %s SET %s = ?, %s= ?, %s = ? WHERE %s = ? AND %s = ?",
                getTableName(), DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION, DBConstant.COLUMN_STATUS,
                DBConstant.COLUMN_ID, DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            statement.setString(2, description);
            statement.setString(3, status.toString());
            statement.setString(4, id);
            statement.setString(5, userId);
            statement.executeUpdate();
            return project;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull Project model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstant.COLUMN_ID, DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION,
                DBConstant.COLUMN_CREATED, DBConstant.COLUMN_USER_ID, DBConstant.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setTimestamp(4, new Timestamp(model.getCreated().getTime()));
            statement.setString(5, model.getUserId());
            statement.setString(6, model.getStatus().toString());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable String userId, @NotNull Project model) {
        model.setUserId(userId);
        return add(model);
    }

}
