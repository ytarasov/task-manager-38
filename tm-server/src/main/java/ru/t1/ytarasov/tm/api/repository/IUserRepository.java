package ru.t1.ytarasov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull Role role);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email, @NotNull Role role);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    void removeByLogin(@Nullable String login);

    boolean isLoginExists(@NotNull String login);

    boolean isEmailExists(@NotNull String email);

    @NotNull
    @SneakyThrows
    User update(@NotNull User user);

}
