package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    int getSize(@NotNull String userId);

    @Nullable
    M add(@Nullable String userId, @NotNull M model);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

}
