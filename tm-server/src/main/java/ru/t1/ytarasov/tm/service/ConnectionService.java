package ru.t1.ytarasov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String dbUrl = propertyService.getDBUrl();
        @NotNull final String dbUsername = propertyService.getDBUsername();
        @NotNull final String dbPassword = propertyService.getDBPassword();
        @NotNull final Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        connection.setAutoCommit(false);
        return connection;
    }

}
