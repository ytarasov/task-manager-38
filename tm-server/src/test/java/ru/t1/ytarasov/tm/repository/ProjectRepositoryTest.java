package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static final String NEW_PROJECT_NAME = "new project";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "new project";

    @NotNull
    private static final String REMOVE_PROJECT_NAME = "remove";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository(connection);

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private static final String userId2 = "FAKE USER ID";

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void createTestUser() {
        @NotNull final String login = "TEST_U2";
        @NotNull final String password = HashUtil.salt(propertyService, "TEST_U2");
        @NotNull final User user = userRepository.add(new User(login, password));
        userId = user.getId();
    }

    @AfterClass
    public static void removeTestUser() {
        @Nullable final User user = userRepository.findOneById(userId);
        userRepository.remove(user);
        userId = "";
    }

    private void createTestDataSet() {
        projectRepository.add(userId, new Project("test project 1", "test project one", Status.IN_PROGRESS));
        projectRepository.add(userId, new Project("test project 2", "test project two", Status.COMPLETED));
    }

    private void clearTestDataSet() {
        if (projectRepository.getSize(userId) > 0) projectRepository.clear(userId);
    }

    @Before
    public void setUp() {
        createTestDataSet();
    }

    @After
    public void tearDown() {
        clearTestDataSet();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, projectRepository.getSize());
    }

    @Test
    public void getSize() {
        final int size1 = projectRepository.findAll().size();
        final int size2 = projectRepository.getSize();
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void findAllWithComparator() {
        @Nullable final List<Project> projectsSorted = projectRepository.findAll(Sort.BY_NAME.getComparator());
        Assert.assertNotNull(projectsSorted);
        final int projectListSize = projectRepository.getSize();
        final int projectSortedRepository = projectsSorted.size();
        Assert.assertEquals(projectListSize, projectSortedRepository);
    }

    @Test
    public void clear() {
        @Nullable List<Project> projects = projectRepository.findAll();
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(projects);
    }

    @Test
    public void findOneById() {
        @NotNull final Project project = new Project(NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        projectRepository.add(project);
        Assert.assertNotNull(project.getId(), projectRepository.findOneById(project.getId()));
        Assert.assertEquals(project.getName(), projectRepository.findOneById(project.getId()).getName());
        Assert.assertNull(projectRepository.findOneById(userId2));
        projectRepository.remove(project);
    }

    @Test
    public void existsById() {
        @NotNull final Project project = new Project("test exists", "test exists");
        projectRepository.add(project);
        Assert.assertEquals(project.getName(), projectRepository.findOneById(project.getId()).getName());
        projectRepository.remove(project);
    }

    @Test
    public void remove() {
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectRepository.add(project);
        Assert.assertEquals(3, projectRepository.findAll().size());
        projectRepository.remove(project);
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    public void addWithUserId() {
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectRepository.add(userId, project);
    }

    @Test
    public void create() {
        final int oldRepositorySize = projectRepository.getSize();
        @NotNull final Project createdProject = projectRepository.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        final int expectedSize = oldRepositorySize + 1;
        final int newRepositorySize = projectRepository.getSize();
        Assert.assertEquals(userId, createdProject.getUserId());
        Assert.assertEquals(expectedSize, newRepositorySize);
        Assert.assertEquals(NEW_PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(NEW_PROJECT_DESCRIPTION, createdProject.getDescription());
    }

    @Test
    public void createWithNameAndStatus() {
        final int oldRepositorySize = projectRepository.getSize();
        @NotNull final Project createdProject = projectRepository.create(userId, NEW_PROJECT_NAME, Status.COMPLETED);
        final int expectedSize = oldRepositorySize + 1;
        final int newRepositorySize = projectRepository.getSize();
        Assert.assertEquals(userId, createdProject.getUserId());
        Assert.assertEquals(expectedSize, newRepositorySize);
        Assert.assertEquals(NEW_PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(Status.COMPLETED, createdProject.getStatus());
    }

    @Test
    public void createWithNameAndDescriptionStatus() {
        final int oldRepositorySize = projectRepository.getSize();
        @NotNull final Project createdProject = projectRepository.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION, Status.COMPLETED);
        final int expectedSize = oldRepositorySize + 1;
        final int newRepositorySize = projectRepository.getSize();
        Assert.assertEquals(userId, createdProject.getUserId());
        Assert.assertEquals(expectedSize, newRepositorySize);
        Assert.assertEquals(NEW_PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(NEW_PROJECT_DESCRIPTION, createdProject.getDescription());
        Assert.assertEquals(Status.COMPLETED, createdProject.getStatus());
    }

    @Test
    public void findAllWithUserId() {
        final int expectedSize = projectRepository.getSize(userId) + 2;
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        final int currentSize = projectRepository.getSize(userId);
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneByIdWithUserId() {
        @NotNull final Project project = projectRepository.create(userId, "test find by id with user");
        @Nullable final Project project2 = projectRepository.findOneById(userId, project.getId());
        @Nullable final Project project3 = projectRepository.findOneById(userId2, project.getId());
        Assert.assertNotNull(project2);
        Assert.assertEquals(project.getId(), project2.getId());
        Assert.assertNull(project3);
    }

    @Test
    public void findAllWithUserIdAndComparator() {
        final int expectedSize = projectRepository.getSize(userId) + 2;
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        final int currentSize = projectRepository.getSize(userId);
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void getSizeWithUserId() {
        final int expectedSize = projectRepository.getSize(userId) + 2;
        projectRepository.create(userId, "test 1 with user Id");
        projectRepository.create(userId, "test 2 with user Id");
        final int currentSize = projectRepository.getSize(userId);
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void removeWithUserId() {
        final int sizeBefore = projectRepository.getSize();
        @Nullable final Project project = projectRepository.create(userId, REMOVE_PROJECT_NAME);
        projectRepository.remove(userId, project);
        final int sizeAfter = projectRepository.getSize();
        Assert.assertEquals(sizeBefore, sizeAfter);
    }

    @Test
    public void clearWithUserId() {
        @Nullable final List<Project> projects = projectRepository.findAll(userId);
        final int fullSize = projectRepository.getSize();
        projectRepository.clear(userId);
        final int currentSize = projectRepository.getSize();
        Assert.assertNotEquals(fullSize, currentSize);
        projectRepository.add(projects);
    }

}
