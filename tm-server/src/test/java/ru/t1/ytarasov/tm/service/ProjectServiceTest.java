package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IProjectService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static final String NEW_PROJECT_NAME = "new project";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "new project";

    @NotNull
    private static final String UPDATE_PROJECT_NAME = "update project";

    @NotNull
    private static final String UPDATE_PROJECT_DESCRIPTION = "update project";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_PROJECT_NAME = "remove";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void createUser() {
        @NotNull final User user = userService.add(new User("test1", "test1"));
        userId = user.getId();
    }

    @AfterClass
    public static void removeUser() throws Exception {
        userService.clear();
    }

    @Before
    public void setup() throws Exception {
        projectService.add(userId, new Project("test project 1"));
        projectService.add(userId, new Project("test project 2"));
        projectService.add(userId, new Project("test project 3"));
    }

    @After
    public void tearDown() throws Exception {
        projectService.clear(userId);
    }

    @Test
    public void findAll() throws Exception {
        final int expectedSize = projectService.getSize();
        @NotNull final List<Project> projects = projectService.findAll();
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithComparator() {
        final int expectedSize = projectService.getSize();
        @Nullable final List<Project> projects = projectService.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(projects);
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithSort() {
        final int expectedSize = projectService.getSize();
        @Nullable final List<Project> projects = projectService.findAll(Sort.BY_STATUS);
        Assert.assertNotNull(projects);
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(""));
        @NotNull final Project project = new Project(NEW_PROJECT_NAME);
        projectService.add(project);
        @Nullable final Project project1 = projectService.findOneById(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        final int expectedSize = projects.size();
        final int currentSize = projectService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final Project project = new Project(CHANGE_STATUS_NAME);
        project.setUserId(userId);
        projectService.add(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, project.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById("", project.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusById(userId, project.getId(), null));
        @Nullable final Project project1 = projectService.changeProjectStatusById(userId, project.getId(), Status.COMPLETED);
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals(project1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws Exception {
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById("", project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, null, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, "", UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, project.getId(), null, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, project.getId(), "", UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, ""));
        @NotNull final Project project1 = projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.remove(null));
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(""));
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectService.add(project);
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void findAllWithUserId() throws Exception {
        @Nullable final String nullUserId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(nullUserId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(""));
        final int expectedSize = projectService.getSize(userId) + 1;
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        @Nullable final List<Project> projects = projectService.findAll(userId);
        Assert.assertNotNull(projects);
        final int currentSIze = projectService.getSize(userId);
        Assert.assertEquals(expectedSize, currentSIze);
    }

    @Test
    public void findAllWithUserIdAndComparator() throws Exception {
        final int expectedSize = projectService.getSize() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(null, Sort.BY_CREATED.getComparator()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll("", Sort.BY_CREATED.getComparator()));
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        @Nullable final List<Project> projects = projectService.findAll(userId, Sort.BY_CREATED.getComparator());
        Assert.assertNotNull(projects);
        final int currentSize = projectService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneByIdAndUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(userId, ""));
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById("", project.getId()));
        @Nullable final Project project1 = projectService.findOneById(userId, project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getName(), project1.getName());
    }

    @Test
    public void getSizeWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(null));
        final int sizeExpected = projectService.getSize(userId) + 1;
        projectService.create(userId, NEW_PROJECT_NAME);
        final int sizeCurrent = projectService.getSize(userId);
        Assert.assertEquals(sizeExpected, sizeCurrent);
    }

    @Test
    public void clear() {
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void clearWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(""));
        projectService.add(userId, new Project("test clear 1"));
        projectService.add(userId, new Project("test clear 2"));
        projectService.add(userId, new Project("test clear 3"));
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.getSize(userId));
    }

}
