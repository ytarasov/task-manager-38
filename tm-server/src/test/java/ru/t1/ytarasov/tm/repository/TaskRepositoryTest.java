package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private static final String NEW_TASK_NAME = "new task";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "new task";

    @NotNull
    private static final String REMOVE_TASK_NAME = "remove";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final ITaskRepository taskRepository = new TaskRepository(connection);

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository(connection);

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void createTestUser() {
        @NotNull final String login = "TEST_U2";
        @NotNull final String password = HashUtil.salt(propertyService, "TEST_U2");
        @NotNull final User user = userRepository.add(new User(login, password));
        userId = user.getId();
    }

    @AfterClass
    public static void removeTestUser() {
        @Nullable final User user = userRepository.findOneById(userId);
        userRepository.remove(user);
        userId = "";
    }

    @Before
    public void setUp() {
        @NotNull final Task task = new Task("test task 1");
        @NotNull final Task task1 = new Task("test task 2");
        taskRepository.add(userId, task);
        taskRepository.add(userId, task1);
    }

    @After
    public void tearDown() {
        if (taskRepository.getSize(userId) > 0) taskRepository.clear(userId);
    }

    @Test
    public void add() {
        final int expectedSize = taskRepository.getSize() + 1;
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertEquals(expectedSize, taskRepository.getSize());
    }

    @Test
    public void addList() {
        final int oldSize = taskRepository.getSize();
        @NotNull final Task task = new Task("test list tasks 1");
        task.setUserId(userId);
        @NotNull final Task task1 = new Task("test list tasks 2");
        task1.setUserId(userId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        tasks.add(task1);
        taskRepository.add(tasks);
        final int expectedSize = tasks.size() + oldSize;
        final int currentSize = taskRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, taskRepository.findAll().size());
    }

    @Test
    public void findAllWithComparator() {
        final int repositorySize = taskRepository.getSize();
        @Nullable final List<Task> tasks = taskRepository.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int sortedListSize = tasks.size();
        Assert.assertEquals(repositorySize, sortedListSize);
    }

    @Test
    public void existsById() {
        Assert.assertFalse(taskRepository.existsById("123"));
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        taskRepository.add(task);
        Assert.assertTrue(taskRepository.existsById(task.getId()));
    }

    @Test
    public void findById() {
        Assert.assertNull(taskRepository.findOneById(""));
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        @NotNull final String taskId = task.getId();
        taskRepository.add(task);
        @Nullable final Task task2 = taskRepository.findOneById(taskId);
        Assert.assertNotNull(task2);
        @NotNull final String taskId2 = task2.getId();
        Assert.assertEquals(taskId, taskId2);
    }

    @Test
    public void remove() {
        @Nullable final Task task = taskRepository.add(new Task(REMOVE_TASK_NAME));
        @Nullable final Task task1 = taskRepository.remove(task);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void clear() {
        @Nullable final List<Task> tasks = taskRepository.findAll();
        taskRepository.clear();
        final int foundSize = taskRepository.getSize();
        Assert.assertEquals(0, foundSize);
        taskRepository.add(tasks);
    }

    @Test
    public void addWithUserId() {
        @Nullable final Task task = taskRepository.add(userId, new Task(NEW_TASK_NAME));
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void getSizeWithUserId() {
        taskRepository.add(userId, new Task(NEW_TASK_NAME));
        final int repositorySize = taskRepository.getSize();
        @Nullable final List<Task> tasks = taskRepository.findAll(userId);
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int foundSize = taskRepository.getSize(userId);
        Assert.assertTrue(foundSize <= repositorySize);
    }

    @Test
    public void findAllWithUserId() {
        final int expectedSize = taskRepository.getSize() + 2;
        taskRepository.add(userId, new Task("task with user id 1"));
        taskRepository.add(userId, new Task("task with user id 2"));
        final int repositorySize = taskRepository.getSize();
        Assert.assertTrue(taskRepository.findAll("").isEmpty());
        @Nullable final List<Task> tasks = taskRepository.findAll(userId);
        Assert.assertNotNull(tasks);
        final int foundSize = tasks.size();
        Assert.assertTrue(foundSize <= repositorySize);
    }

    @Test
    public void findAllWithUserIdAndComparator() {
        taskRepository.add(userId, new Task(NEW_TASK_NAME));
        final int repositorySize = taskRepository.getSize(userId);
        @Nullable final List<Task> tasks = taskRepository.findAll(userId, Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int sortedListSize = tasks.size();
        Assert.assertEquals(repositorySize, sortedListSize);
    }

    @Test
    public void create() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
    }

    @Test
    public void createWithNameAndDescription() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, task.getDescription());
    }

    @Test
    public void createWithNameAndStatus() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME, Status.IN_PROGRESS);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void createWithNameAndDescriptionAndStatus() {
        final int sizeBefore = taskRepository.getSize();
        @Nullable final Task task = taskRepository.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION, Status.IN_PROGRESS);
        final int sizeExpected = sizeBefore + 1;
        final int sizeAfter = taskRepository.getSize();
        Assert.assertEquals(sizeExpected, sizeAfter);
        Assert.assertEquals(NEW_TASK_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, task.getDescription());
    }

    @Test
    public void findByProjectId() {
        @NotNull final Project project = projectRepository.create(userId, "project with task");
        final int expectedSize = taskRepository.getSize(userId) - 1;
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        @NotNull final String taskId = task.getId();
        task.setUserId(userId);
        task.setProjectId(project.getId());
        taskRepository.add(task);
        @Nullable final List<Task> tasks = taskRepository.findAllTasksByProjectId(userId, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertNotEquals(0, tasks.size());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(expectedSize, tasks.size());
        boolean foundFlag = false;
        for (Task task1 : tasks) {
            @NotNull final String taskId1 = task1.getId();
            foundFlag = taskId1.equals(taskId);
            if (foundFlag) break;
        }
        Assert.assertTrue(foundFlag);
        for (Task oneTask : tasks) {
            taskRepository.remove(oneTask);
        }
        projectRepository.remove(project);
    }

    @Test
    public void removeWithUserId() {
        @Nullable final Task task = taskRepository.create(userId, REMOVE_TASK_NAME);
        @NotNull final String taskId = task.getId();
        @Nullable final Task task1 = taskRepository.remove(userId, task);
        Assert.assertNotNull(task1);
        @NotNull final String taskId1 = task1.getId();
        Assert.assertEquals(taskId, taskId1);
    }

    @Test
    public void clearWithUserId() {
        taskRepository.create(userId, "test clear with user id 1");
        taskRepository.create(userId, "test clear with user id 2");
        final int sizeBefore = taskRepository.getSize();
        final int sizeWithUserIdBefore = taskRepository.getSize(userId);
        taskRepository.clear(userId);
        final int expectedSize = sizeBefore - sizeWithUserIdBefore;
        final int foundSize = taskRepository.getSize();
        Assert.assertEquals(expectedSize, foundSize);
        Assert.assertEquals(0, taskRepository.getSize(userId));
    }

}
