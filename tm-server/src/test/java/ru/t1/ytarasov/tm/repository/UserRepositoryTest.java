package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(connection);

    @Before
    public void setup() {
        createUsers();
    }

    @After
    public void tearDown() {
        userRepository.removeByLogin("test2");
        userRepository.removeByLogin("test3");
        userRepository.removeByLogin("test4");
        userRepository.removeByLogin("test");
        userRepository.removeByLogin("admin");
    }

    @Test
    public void add() {
        final int firstSize = userRepository.getSize();
        userRepository.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        final int expectedSize = firstSize + 1;
        final int currentSize = userRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void addList() {
        final int firstSize = userRepository.getSize();
        @NotNull final List<User> users = new ArrayList<>();
        users.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        users.add(new User("test3", HashUtil.salt("test3", "452316", 7657)));
        users.add(new User("test4", HashUtil.salt("test4", "452316", 7657)));
        userRepository.add(users);
        final int expectedSize = firstSize + users.size();
        final int currentSize = userRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void existsById() {
        @NotNull final User user = userRepository.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        @NotNull final String id = user.getId();
        Assert.assertTrue(userRepository.existsById(id));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(userRepository.getSize(), userRepository.findAll().size());
    }

    @Test
    public void getSize() {
        final int size1 = userRepository.findAll().size();
        final int size2 = userRepository.getSize();
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void findOneById() {
        @NotNull final User user = userRepository.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        @Nullable final User user1 = userRepository.findOneById(user.getId());
        Assert.assertNotNull(user1);
        Assert.assertEquals(user.getLogin(), user1.getLogin());
    }

    @Test
    public void remove() {
        @NotNull final User user = userRepository.add(new User("trt", HashUtil.salt("trt", "452316", 7657)));
        final int expectedSize = userRepository.getSize() - 1;
        userRepository.remove(user);
        final int currentSize = userRepository.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void clear() {
        userRepository.clear();
        Assert.assertEquals(0, userRepository.getSize());
    }

    private void createUsers() {
        @Nullable final String passwordHash = HashUtil.salt("test", "452316", 7657);
        Assert.assertNotNull(passwordHash);
        @NotNull final User user = new User("test", passwordHash, "test@test.com");
        userRepository.add(user);
        @Nullable final String passwordHashAdmin = HashUtil.salt("admin", "452316", 7657);
        @NotNull final User user1 = new User("admin", passwordHashAdmin, Role.ADMIN);
        user1.setEmail("admin@test.com");
        userRepository.add(user1);
    }

}
