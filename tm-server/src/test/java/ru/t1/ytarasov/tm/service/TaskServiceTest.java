package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static final String NEW_TASK_NAME = "new task";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "new task";

    @NotNull
    private static final String UPDATE_TASK_NAME = "update task";

    @NotNull
    private static final String UPDATE_TASK_DESCRIPTION = "update task";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_TASK_NAME = "remove";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void createUser() throws Exception {
        @NotNull final User user = userService.create("test26", "test2");
        userId = user.getId();
    }

    @AfterClass
    public static void removeUser() throws Exception {
        projectService.clear(userId);
        userService.clear();
    }

    @Before
    public void setup() throws Exception {
        taskService.add(userId, new Task("test task 1"));
        taskService.add(userId, new Task("test task 2"));
        taskService.add(userId, new Task("test task 3"));
    }

    @After
    public void tearDown() throws Exception {
        taskService.clear();
    }

    @Test
    public void findAll() throws Exception {
        final int expectedSize = taskService.getSize();
        @NotNull final List<Task> tasks = taskService.findAll();
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithComparator() {
        final int expectedSize = taskService.getSize();
        @Nullable final List<Task> tasks = taskService.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithSort() {
        final int expectedSize = taskService.getSize();
        @Nullable final List<Task> tasks = taskService.findAll(Sort.BY_STATUS);
        Assert.assertNotNull(tasks);
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(""));
        @NotNull final Task task = new Task(NEW_TASK_NAME);
        taskService.add(task);
        @Nullable final Task task1 = taskService.findOneById(task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int currentSize = taskService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final Task task = new Task(CHANGE_STATUS_NAME);
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById("", task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById(userId, task.getId(), null));
        @Nullable final Task task1 = taskService.changeTaskStatusById(userId, task.getId(), Status.COMPLETED);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(task1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws Exception {
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById("", task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, null, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, "", UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), null, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), "", UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, ""));
        @NotNull final Task task1 = taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.remove(null));
        @NotNull final Task task = new Task(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(""));
        @NotNull final Task task = new Task(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void findAllWithUserId() throws Exception {
        @Nullable final String nullUserId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(nullUserId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(""));
        final int expectedSize = taskService.getSize(userId) + 1;
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        final int currentSize = taskService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithUserIdAndComparator() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(null, Sort.BY_CREATED.getComparator()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll("", Sort.BY_CREATED.getComparator()));
        final int expectedSize = taskService.getSize(userId) + 1;
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        @Nullable final List<Task> tasks = taskService.findAll(userId, Sort.BY_CREATED.getComparator());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(expectedSize, tasks.size());
    }

    @Test
    public void findOneByIdAndUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, ""));
        @NotNull final Task task = taskService.create(userId, NEW_TASK_NAME);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(null, task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById("", task.getId()));
        @Nullable final Task task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getName(), NEW_TASK_NAME);
    }

    @Test
    public void getSizeWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(null));
        final int sizeExpected = taskService.getSize(userId) + 1;
        taskService.create(userId, NEW_TASK_NAME);
        final int sizeCurrent = taskService.getSize(userId);
        Assert.assertEquals(sizeExpected, sizeCurrent);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        @NotNull final Project project = new Project("test project");
        project.setId(project.getId());
        project.setUserId(userId);
        projectService.add(project);
        @Nullable final Task task = taskService.create(userId, "test bind to project");
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), project.getId());
        @Nullable final Task task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        @NotNull final Project project = new Project("test project");
        project.setId(project.getId());
        project.setUserId(userId);
        projectService.add(project);
        @Nullable final Task task = taskService.create(userId, "test bind to project");
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), project.getId());
        @Nullable final Task task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void clearWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(""));
        @Nullable final List<Task> tasks = taskService.findAll(userId);
        taskService.add(userId, new Task("test clear 1"));
        taskService.add(userId, new Task("test clear 2"));
        taskService.add(userId, new Task("test clear 3"));
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.getSize(userId));
        Assert.assertNotNull(tasks);
        taskService.add(tasks);
    }

}
