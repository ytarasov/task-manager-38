package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.ExistsEmailException;
import ru.t1.ytarasov.tm.exception.user.ExistsLoginException;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.repository.UserRepository;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);

    @BeforeClass
    public static void setup() {
        @Nullable final String passwordHash = HashUtil.salt("test", "452316", 7657);
        Assert.assertNotNull(passwordHash);
        @NotNull final User user = new User("test", passwordHash, "test@test.com");
        userService.add(user);
        @Nullable final String passwordHashAdmin = HashUtil.salt("admin", "452316", 7657);
        @NotNull final User user1 = new User("admin", passwordHashAdmin, Role.ADMIN);
        user1.setEmail("admin@test.com");
        userService.add(user1);;
    }

    @AfterClass
    public static void tearDown() throws Exception {
        userService.removeByLogin("test2");
        userService.removeByLogin("admin");
    }

    @Test
    public void add() throws Exception {
        final int firstSize = userService.getSize();
        userService.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        final int expectedSize = firstSize + 1;
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
        userService.removeByLogin("test2");
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final User user = userService.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        @NotNull final String id = user.getId();
        Assert.assertTrue(userService.existsById(id));
        userService.removeByLogin("test2");
    }

    @Test
    public void findAll() throws Exception {
        Assert.assertEquals(userService.getSize(), userService.findAll().size());
    }

    @Test
    public void getSize() throws Exception {
        final int size1 = userService.findAll().size();
        final int size2 = userService.getSize();
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
        @NotNull final User user = userService.add(new User("test2", HashUtil.salt("test2", "452316", 7657)));
        @NotNull final String id = user.getId();
        @Nullable final User user1 = userService.findOneById(id);
        Assert.assertNotNull(user1);
        Assert.assertEquals(id, user1.getId());
        userService.removeByLogin("test2");
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        userService.add(new User("test24", "test24"));
        @Nullable final User user = userService.findByLogin("test");
        Assert.assertNotNull(user);
    }

    @Test
    public void registry() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, "test12", "test12@test.ru"));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "test", "test12@test.ru"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("test12", null, "test12@test.ru"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("test12", "", "test12@test.ru"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create("test12", "test", ""));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("test", "test12", "test12@test.ru"));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create("test12", "test", "test@test.com"));
        final int expectedSize = userService.getSize() + 1;
        @NotNull final User user = userService.create("test12", "test12", "test12@test.ru");
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
        userService.removeByLogin("test12");
    }

    @Test
    public void changePassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, "test2"));
        @Nullable final User user = userService.create("test26", "test26");
        Assert.assertNotNull(user);
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), ""));
        @Nullable final User user1 = userService.setPassword(user.getId(), "test2");
        Assert.assertEquals(user.getId(), user1.getId());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.remove(null));
        @NotNull final User user = userService.add(new User("trt", HashUtil.salt("trt", "452316", 7657)));
        final int expectedSize = userService.getSize() - 1;
        userService.remove(user);
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
        @NotNull final User user = userService.add(new User("trt", HashUtil.salt("trt", "452316", 7657)));
        final int expectedSize = userService.getSize() - 1;
        userService.removeById(user.getId());
        final int currentSize = userService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
        userService.add(users);
    }

}
